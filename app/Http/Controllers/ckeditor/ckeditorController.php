<?php

namespace App\Http\Controllers\ckeditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ckeditor\Ckeditor;
use DB;

class ckeditorController extends Controller
{
    //
    public function index(){
      $q = request()->input('q');
      $ckeditors = DB::table('ckeditors')
          ->paginate(5);

      //return view('ckeditor.index');
      return view('ckeditor.index',compact('ckeditors'))
                        ->with('i', (request()->input('page', 1) - 1) * 5);;
    }

    public function store(Request $request){
      $data = ['soal' => $request->soal,
              'jawaban' => $request->jawaban];
      Ckeditor::create($data);

      return redirect()->route('ckeditor.index')
                      ->with('success', 'Simpan berhasil.');
    }
}
