<?php

namespace App\ckeditor;

use Illuminate\Database\Eloquent\Model;

class Ckeditor extends Model
{
    //
    public $timestamps = false;
    
    protected $fillable = [
        'soal', 'jawaban'
    ];
}
