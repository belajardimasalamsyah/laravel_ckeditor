<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Belajar CKeditor</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
    <div class="container">

      <h1>Belajar CKeditor</h1>
      <h6>Dimas Alamsyah</h6>

      <form action="{{route('ckeditor.store')}}" method="post" style="padding: 15px">
        {{ csrf_field() }}

        @if ($message = Session::get('success'))
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
        @endif

        <input class="form-control" type="text" name="soal" value="" placeholder="Soal" style="margin-bottom: 15px">
        <textarea class="form-control" name="jawaban" id="editor" rows="8" cols="80" style="margin-bottom: 15px"></textarea>
        <button type="submit" name="button" class="btn btn-success" style="margin-top: 15px">Simpan</button>
      </form>

    <hr>

      @foreach ($ckeditors as $ckeditor)
        <span>{{ ++$i }}.</span>
        <span>{{ $ckeditor->soal }}</span>
        <span>{!! $ckeditor->jawaban !!}</span>
        <br>
      @endforeach

    </div>

  </body>

  <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
  <script>
      CKEDITOR.replace( 'editor' );
  </script>

</html>
